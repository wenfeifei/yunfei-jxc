package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.PurchaseOrder;

import java.util.Map;

/**
 * ClassName: PurchaseOrderService
 * Description: 采购单信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PurchaseOrderService extends BaseService<PurchaseOrder, String> {

    public Page<PurchaseOrder> queryPage(Map<String, Object> params);

    public int modifyWithUnLock(String id, PurchaseOrder purchaseOrder);

    public PurchaseOrder queryTotalAmount(String orgId, int status, int payStatus);

    //public int modifyWithCheck(String id);
}