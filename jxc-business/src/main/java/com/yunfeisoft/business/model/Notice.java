package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ClassName: Notice
 * Description: 公告信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-23
 */
@Entity
@Table(name = "TT_NOTICE")
public class Notice extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 标题
     */
    @Column
    private String title;

    /**
     * 栏目
     */
    @Column
    private String type;

    /**
     * 视频地址
     */
    @Column
    private String videoUrl;

    /**
     * 排序
     */
    @Column
    private Integer num;

    private String content;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}