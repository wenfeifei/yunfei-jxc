<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>商品列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" style="margin:5px;">
        <div style="color:#FF5722;font-weight:bold;position:absolute;right:15px;top:15px;z-index:100">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('product_trace')">使用技巧</button>
            当前查看商品：{{productName || "请选择商品信息"}}
        </div>
        <div class="layui-tab layui-tab-card" style="margin:0px;">
            <ul class="layui-tab-title">
                <li class="layui-this" @click="showFrame(1)">商品信息</li>
                <li @click="showFrame(2)">进货记录</li>
                <li @click="showFrame(3)">销售记录</li>
                <li @click="showFrame(4)">调拨记录</li>
                <li @click="showFrame(5)">损益记录</li>
                <li @click="showFrame(6)">库存变化历史</li>
            </ul>
            <div class="layui-tab-content" style="padding:0px;">
                <div :class="index == 1 ? 'layui-tab-item layui-show' : 'layui-tab-item'" v-show="index == 1">
                    <iframe src="${params.contextPath}/view/business/product/product_list_show.htm" id="product-frame" width="100%" frameborder="0"></iframe>
                </div>
                <div :class="index != 1 ? 'layui-tab-item layui-show' : 'layui-tab-item'" v-show="index != 1">
                    <iframe src="javascript:void(0)" id="frame" width="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            productName: '',
            productId: '',
            index: 1
        },
        mounted: function () {
            this.init();
        },
        methods: {
            init:function () {
                $("#frame, #product-frame").height($(window).height() - 60);
            },
            showFrame: function (index) {
                this.index = index;
                console.log(index);
                if (index == "1") {
                    return;
                }
                if (!this.productId) {
                    $.message("请选择商品");
                    return;
                }
                var data = {
                    '2': '${params.contextPath}/view/business/purchaseItem/purchaseItem_list.htm',//进货记录
                    '3': '${params.contextPath}/view/business/saleItem/saleItem_list.htm',//销售记录
                    '4': '${params.contextPath}/view/business/allotItem/allotItem_list.htm',//调拨记录
                    '5': '${params.contextPath}/view/business/indecItem/indecItem_list.htm',//损益记录
                    '6': '${params.contextPath}/view/business/product/stock_change_list.htm',//库存变化历史
                };
                $("#frame").attr("src", data[index] + "?productId=" + this.productId);
            },
            selectProduct: function (product) {
                this.productName = product.name || "";
                this.productId = product.id || "";
                $.message("已选择商品：" + this.productName);
            }
        }
    });
</script>
</body>

</html>
